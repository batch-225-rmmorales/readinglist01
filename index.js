// 1. create a function that will add two numbers
function addTwo(a, b) {
    console.log(a + " + " + b + " = " + (a+b));
}
addTwo(5,3);

// 2. create a function that will subtract two numbers
function diffTwo(a, b) {
    console.log(a + " - " + b + " = " + (a-b));
}
diffTwo(5,3);
// 3. create a function that will display a user's top 3 favorite movie characters
function displayTopThree(){
    let topOne = prompt('what is your #1 movie');
    let topTwo = prompt('what is your #2 movie');
    let topThree = prompt('what is your #3 movie');
    console.log("Your top 3 movies are " + topOne + ", " + topTwo + ", " + topThree)

}
displayTopThree();


// 4. create a function that calculates a dogs age based on the ratio 1/7 "your doggo is _ in human years!"

function humanYears() {
    let dogAge = prompt('how old is your dog?')
    console.log('Your doggo is ' + dogAge*7 + ' in human years!');
}
humanYears();

//5. create a function that takes 3 subject grades and return the average "your average is _"

function averageGrade(a,b,c) {
    console.log('your average grade is ' + (a+b+c)/3);
}
averageGrade(85, 65, 90);